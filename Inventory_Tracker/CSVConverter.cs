﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Inventory_Tracker
{
   public class CSVConverter : IConverter
    {
        StreamWriter writer;
        StreamReader reader;

        public CSVConverter()
        {
        }

        public void ConvertTo(Inventory userData)
        {
            if (File.Exists("inventory.csv"))
            {
                using (writer = new StreamWriter("inventory.csv", append: true))
                {
                    writer.WriteLine(userData.Name + '^' + userData.Rating);
                }
            }
            else
            {
                using (writer = new StreamWriter("inventory.csv"))
                {
                    writer.WriteLine(userData.Name + '^' + userData.Rating);
                }
            }
            
        }

        public List<Inventory> ConvertFrom()
        {
            List<Inventory> inventory = new List<Inventory>();
            int counter = 0;
            if (File.Exists("inventory.csv"))
            {
                using (reader = new StreamReader("inventory.csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] line = reader.ReadLine().Split('^');
                        inventory.Add(new Inventory());
                          inventory[counter].Name = line[0];
                        inventory[counter].Rating = decimal.Parse(line[1]);
                        counter++;
                    }
                }
            
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("There are currently no items; please submit an item");
            }
            return inventory;
        }
    }
}
