﻿using System.Collections.Generic;

namespace Inventory_Tracker
{

    interface IConverter
    {

        void ConvertTo(Inventory inv);
        List<Inventory> ConvertFrom();

       // IEnumerator<Inventory> GetEnumerator(Inventory userData);


    }


}
