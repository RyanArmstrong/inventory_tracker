﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;



namespace Inventory_Tracker
{
    public partial class FrmHome : Form
    {
        XMLItems items = XMLItems.XmlItems;
        IConverter csv = new CSVConverter();
        IConverter xml = new XMLConverter();
        public FrmHome()
        {
            InitializeComponent();
            
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Inventory userData = new Inventory();

            userData.Name = txtboxSubmitName.Text;
            userData.Rating = numericRating.Value;
            
            if (radCSV.Checked == true)
            {
                csv.ConvertTo(userData);
            }
            else if (radXML.Checked)
            {
                xml.ConvertTo(userData);
            }

        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            txtboxDisplay.Text = "";
            txtboxDisplay.Enabled = true;
            if (radCSV.Checked)
            {
                foreach (var item in csv.ConvertFrom())
                    txtboxDisplay.Text += "Product Name: " + item.Name + "\n" + "Product Rating: " + item.Rating + "\n";
            }
            else if (radXML.Checked)
            {
                foreach (var item in xml.ConvertFrom())
                {
                    txtboxDisplay.Text += "Product Name: " + item.Name + "\n" + "Product Rating: " + item.Rating + "\n";
                }
            }
        }
    }
}
