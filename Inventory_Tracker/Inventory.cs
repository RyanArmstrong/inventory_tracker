﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Inventory_Tracker
{
    [Serializable]
  public  class Inventory
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Rating")]
        public decimal Rating { get; set; }

        public Inventory()
        {
            Name = "";
            Rating = 0m;

        }

        public Inventory(string _name, decimal _rating)
        {
            _name = Name;
            _rating = Rating;
        }
    }


}
