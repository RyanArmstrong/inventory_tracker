﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Inventory_Tracker
{
    [Serializable]

    public class XMLConverter : IConverter
    {
     
        public void ConvertTo(Inventory userData)
        {
            XMLItems xMLItems = XMLItems.XmlItems; 
            xMLItems.CollectionName = "Inventory Items";
            xMLItems.Add(userData);
            XmlSerializer serializer = new XmlSerializer(typeof(XMLItems));
            TextWriter writer;
            if (File.Exists("inventory.xml"))
            {
                using (writer = new StreamWriter("inventory.xml", append: false))
                {
                    serializer.Serialize(writer, xMLItems);
                }
            }
            else
            {
                using (writer = new StreamWriter("inventory.xml"))
                {
                    serializer.Serialize(writer, xMLItems);
                }
            }

            
            //if (File.Exists("inventory.xml"))
            //    using (writer = new StreamWriter("inventory.xml", append: true))
            //        serializer.Serialize(writer, userData);
            //else
            //    using (writer = new StreamWriter("inventory.xml"))
            //        serializer.Serialize(writer, userData);

        }

        public List<Inventory> ConvertFrom()
        {
            List<Inventory> ReadfromXML = new List<Inventory>();
            string [] UneditedList = new string [5];
            XmlSerializer deserializer = new XmlSerializer(typeof(XMLItems));


            if (File.Exists("inventory.xml"))
            {
                using (var fileStream = File.Open("inventory.xml", FileMode.Open, FileAccess.Read))
                using (var reader = new StreamReader(fileStream))
                {
                    XMLItems it = (XMLItems)deserializer.Deserialize(fileStream);
                    return it.ToList();
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("There are currently no items; please submit an item");
            }
            return ReadfromXML;
        }
    }
}
