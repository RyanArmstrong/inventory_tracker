﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Inventory_Tracker
{
    public class XMLItems: ICollection<Inventory>
    {
        private List<Inventory> userData;
        public string CollectionName = "DEFAULT NAME";
        public bool readOnly = false;
        public static XMLItems xmlItems = null;
        private XMLItems()
        {
            userData = new List<Inventory>();

        }

        public static XMLItems XmlItems
        {
            get
            {
                if (xmlItems == null)
                    xmlItems = new XMLItems();
                return xmlItems;
            }
        }

        public Inventory this[int index]
        {
            get { return userData[index]; }
        }

        public bool IsReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }

        public void CopyTo(Inventory[] target, int index)
        {
            userData.ToArray().CopyTo(target, index);
        }

        public int Count
        {
            get { return userData.Count(); }
        }

        public  void Add(Inventory inventory)
        {
            userData.Add(inventory);
        }

        public bool Contains(Inventory inventory)
        {
            if (userData.Contains(inventory))
            {
                return true;
            }
            return false;
        }

        public bool Remove(Inventory inventory)
        {
            if (Contains(inventory))
            {
                userData.Remove(inventory);
                return true;
            }
            return false;
        }

        public void Clear()
        {
            userData = new List<Inventory>();
        }

        public IEnumerator<Inventory> GetEnumerator()
        {
            return userData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return userData.GetEnumerator();
        }
    }
}
