﻿namespace Inventory_Tracker
{
    partial class FrmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHome));
            this.txtboxDisplay = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtboxSubmitName = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radXML = new System.Windows.Forms.RadioButton();
            this.radCSV = new System.Windows.Forms.RadioButton();
            this.toolTipCSV = new System.Windows.Forms.ToolTip(this.components);
            this.numericRating = new System.Windows.Forms.NumericUpDown();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRating)).BeginInit();
            this.SuspendLayout();
            // 
            // txtboxDisplay
            // 
            this.txtboxDisplay.Enabled = false;
            this.txtboxDisplay.Location = new System.Drawing.Point(0, 426);
            this.txtboxDisplay.Name = "txtboxDisplay";
            this.txtboxDisplay.Size = new System.Drawing.Size(425, 244);
            this.txtboxDisplay.TabIndex = 0;
            this.txtboxDisplay.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter New Inventory Item";
            // 
            // txtboxSubmitName
            // 
            this.txtboxSubmitName.Location = new System.Drawing.Point(149, 57);
            this.txtboxSubmitName.Name = "txtboxSubmitName";
            this.txtboxSubmitName.Size = new System.Drawing.Size(251, 26);
            this.txtboxSubmitName.TabIndex = 0;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(123, 303);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(144, 52);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(123, 368);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(144, 52);
            this.btnDisplay.TabIndex = 4;
            this.btnDisplay.Text = "&Display Current Inventory";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Rating";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Convert to File Type:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radXML);
            this.panel1.Controls.Add(this.radCSV);
            this.panel1.Location = new System.Drawing.Point(182, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 66);
            this.panel1.TabIndex = 2;
            this.toolTipCSV.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // radXML
            // 
            this.radXML.AutoSize = true;
            this.radXML.Location = new System.Drawing.Point(3, 33);
            this.radXML.Name = "radXML";
            this.radXML.Size = new System.Drawing.Size(60, 24);
            this.radXML.TabIndex = 1;
            this.radXML.Text = "XML";
            this.radXML.UseVisualStyleBackColor = true;
            // 
            // radCSV
            // 
            this.radCSV.AutoSize = true;
            this.radCSV.Checked = true;
            this.radCSV.Location = new System.Drawing.Point(3, 3);
            this.radCSV.Name = "radCSV";
            this.radCSV.Size = new System.Drawing.Size(60, 24);
            this.radCSV.TabIndex = 0;
            this.radCSV.TabStop = true;
            this.radCSV.Text = "CSV";
            this.radCSV.UseVisualStyleBackColor = true;
            // 
            // toolTipCSV
            // 
            this.toolTipCSV.ToolTipTitle = "Comma Seperated Values stored in an Excel Document";
            // 
            // numericRating
            // 
            this.numericRating.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numericRating.Location = new System.Drawing.Point(149, 106);
            this.numericRating.Name = "numericRating";
            this.numericRating.ReadOnly = true;
            this.numericRating.Size = new System.Drawing.Size(52, 26);
            this.numericRating.TabIndex = 1;
            // 
            // FrmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 713);
            this.Controls.Add(this.numericRating);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtboxSubmitName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtboxDisplay);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmHome";
            this.Text = "Inventory Data Storage";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRating)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtboxDisplay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtboxSubmitName;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radXML;
        private System.Windows.Forms.RadioButton radCSV;
        private System.Windows.Forms.ToolTip toolTipCSV;
        private System.Windows.Forms.NumericUpDown numericRating;
    }
}

